angular.module('starter.controllers', []);
angular.module('starter.services', []);

angular.module('starter', ['ionic', 'ionic.service.core', 'ionic.service.push', 'starter.controllers', 'starter.services', 'uiGmapgoogle-maps', 'nemLogging', 'angular-oauth2', 'ngResource'])

.constant('appConfig', {
	baseUrl: 'http://linkedpet.com.br',
	localUrl: 'http://localhost:8000'
})

.constant('authData', {
	grant_type: 'password',
	username: 'raulvbrito@gmail.com',
	password: '12345',
	client_id: 'appid01',
	client_secret: 'secret'
})

.run(function($ionicPlatform, $http, $cordovaPush, $rootScope) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    
	if(window.StatusBar) {
      StatusBar.style(1);
    }
	
	var push = new Ionic.Push({
      "debug": false
    });
 
    push.register(function(token) {
      console.log("My Device token:",token.token);
      push.saveToken(token);
    });
	
//	var iosConfig = {
//	 "badge": true,
//	 "sound": true,
//	 "alert": true,
//	};
//	
//	setTimeout(function(){
//		alert('antes do registro do device token');
//		$cordovaPush.register(iosConfig).then(function(deviceToken) {
//			alert('registro do device token');
//			console.log(deviceToken);
//			$http.post("https://push.ionic.io/api/v1/push",
//				{ "tokens":[ deviceToken ],
//				  "production": true,
//				  "notification":{
//					"alert":"Hello World!",
//					"ios":{
//						"badge":1,
//						"sound":"ping.aiff",
//						"expiry": 1423238641,
//						"priority": 10,
//						"contentAvailable": true,
//						"payload":{
//							"key1":"value",
//							"key2":"value"
//						}
//					}
//				}
//			});
//		}, function(error){
//			console.log(error);
//			alert('Registration Error');
//		});
//	}, 5000);
	
//	$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
//		console.log(notification);
//		if (notification.alert) {
//			navigator.notification.alert(notification.alert);
//		}
//										   
//		if (notification.sound) {
//			var snd = new Media(event.sound);
//			snd.play();
//		}
//		
//		if (notification.badge) {
//			$cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
//				console.log(result);
//			}, function(err) {
//				console.log(err);
//			});
//		}
//	});
  });
})

.config(function($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider, OAuthProvider, OAuthTokenProvider, appConfig, $ionicAppProvider, $provide){
		$ionicAppProvider.identify({
			app_id: '6014ec9b',
			api_key: '60302388fde1b41ebe0c6ca0c1c48179083aa6489de2481b',
			dev_push: false
		});
		
		OAuthProvider.configure({
			baseUrl: appConfig.baseUrl,
			clientId: 'appid01',
			clientSecret: 'secret'
		});
		
		OAuthTokenProvider.configure({
			name: 'token',
			options: {
				secure: false
			}
		});
		
        $stateProvider
        .state('app',{
               url: '/app',
               abstract: true,
               templateUrl: 'templates/menu.html',
               controller: 'AppCtrl'
        })
        .state('login', {
               url: '/login',
               templateUrl: 'templates/login.html',
               controller: 'LoginCtrl'
        })
        .state('app.home',{
               url: '/home',
               views: {
                'menuContent': {
                    templateUrl: 'templates/home.html',
                    controller: 'HomeCtrl'
                }
               }
        })
        .state('app.servicos',{
               url: '/servicos',
               views: {
                'menuContent': {
                    templateUrl: 'templates/servicos.html',
                    controller: 'ServicosCtrl'
                }
               }
        })
        .state('app.servico',{
               url: '/servico',
               views: {
                'menuContent': {
                    templateUrl: 'templates/servico.html',
                    controller: 'ServicoCtrl'
                }
               }
        })
        .state('app.petshops',{
               url: '/petshops',
               views: {
                'menuContent': {
                    templateUrl: 'templates/petshops.html',
                    controller: 'PetshopsCtrl'
                }
               }
        })
        .state('app.petshop',{
               url: '/petshops/:petshopId',
			   cache: false,
               views: {
                'menuContent': {
                    templateUrl: 'templates/petshop.html',
                    controller: 'PetshopCtrl'
                }
               }
        })
		.state('app.agendamentos',{
               url: '/agendamentos',
               views: {
                'menuContent': {
                    templateUrl: 'templates/agendamentos.html',
                    controller: 'SchedulesCtrl'
                }
               }
        })
        .state('app.pets',{
               url: '/pets',
               views: {
                'menuContent': {
                    templateUrl: 'templates/pets.html',
                    controller: 'PetsCtrl'
                }
               }
        });
        
        var user_info = localStorage.getItem('user_info');
        var initial_route;
        
        if(user_info)
            initial_route = "/app/servicos";
        else
            initial_route = "/login";
        
        $urlRouterProvider.otherwise(initial_route.toString());
        
        uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyBGBgInlseBkvuGrSyM7nKW3WBLAb54Qu4',
                v: '3.17',
                libraries: 'weather,geometry,visualization'
        });
		
		$provide.decorator('$state', function($delegate, $stateParams) {
			$delegate.forceReload = function() {
				return $delegate.go($delegate.current, $stateParams, {
					reload: true,
					inherit: false,
					notify: true
				});
			};
			return $delegate;
		});
})

.directive('iconSwitcher', function() {
	   return {
		restrict : 'A',
		link : function(scope, elem, attrs) {
			var currentState = true;
			elem.on('click', function() {
				   if(currentState === true) {
					angular.element(elem).removeClass(attrs.onIcon);
					angular.element(elem).addClass(attrs.offIcon);
				   } else {
					angular.element(elem).removeClass(attrs.offIcon);
					angular.element(elem).addClass(attrs.onIcon);
				   }
				   
				   currentState = !currentState
			});
	   }
	  };
}); 