angular.module('starter.services', []);

angular.module('starter.controllers', ['ngCordova', 'starter.services'])

.controller('LoginCtrl', function($scope, $state, $http, $ionicModal, $ionicPopup, $cordovaOauth, $ionicLoading, OAuth, User, FacebookUserCheck, FacebookUserInsert, appConfig, $ionicUser, $ionicPush, $rootScope, $ionicActionSheet, $cordovaImagePicker, authData, $cordovaCamera, $cordovaFileTransfer, $cordovaPush, $cordovaGeolocation){
            $scope.login = function(loginData){
				$ionicLoading.show();
				var authData = {
					'grant_type': 'password',
					'username' : loginData.ds_login,
					'password' : loginData.ds_senha,
					'client_id': 'appid01',
					'client_secret': 'secret'
				};
			
				$scope.authData = authData;
			
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl+'/api/users/user/'+loginData.ds_login+'?access_token='+data.data.access_token).then(function(data){
						authData.image = data.data[0].image;
						authData.name = data.data[0].name;
						
						localStorage.setItem('user_info', JSON.stringify(authData));
					}, function(error){
						console.log(error);
						$ionicLoading.hide();
					});
					
					$scope.identifyUser();
				}, function(error){
					console.log(error);
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: 'Erro de Autenticação',
						template: 'Login ou Senha incorretos'
					});
				});
            }
			
			$rootScope.$on('$cordovaPush:tokenReceived', function(event, data){
				console.log('entrou na função tokenReceived');
				console.log(data.token);
				
				var deviceToken = { 'device_token': data.token };
				var authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.post(appConfig.baseUrl+'/api/users/device_token?access_token='+data.data.access_token, JSON.stringify(deviceToken)).then(function(data){
						localStorage.setItem('device_token', data.token);
					}, function(error){
						console.log(error);
					});
				});
			});
			
			$scope.identifyUser = function() {
				var user = $ionicUser.get();
				if(!user.user_id) {
					user.user_id = $ionicUser.generateGUID();
				}

				angular.extend(user, {
					name: 'Ionitron',
					bio: 'I come from planet Ion'
				});
			
//				$ionicUser.identify(user).then(function(){
//					$scope.identified = true;
//					console.log('Identified user ' + user.name + '\n ID ' + user.user_id);
					$scope.pushRegister();
//				});
            }
			
			$scope.pushRegister = function() {
				$ionicPush.register({
					canShowAlert: true,
					canSetBadge: true,
					canPlaySound: true,
					canRunActionsOnWake: true,
					onNotification: function(notification) {
						console.log(notification);
						$ionicPopup.alert({
							title: notification.alert.split('|')[0],
							template: notification.alert.split('|')[1]
						});
						return true;
					}
				});

				$ionicLoading.hide();
			
				var posOptions = {timeout: 5000, enableHighAccuracy: true};
				$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
					var lat  = position.coords.latitude;
					var long = position.coords.longitude;
					
					$http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat.replace(',', '.')+','+long.replace(',', '.')+'&key=AIzaSyCmUWdqp6dWn_8HCR-3nxdhKxGYxgY86qs').then(function(resp) {
						localStorage.setItem('city', resp.data.results[9].address_components[0].long_name);
					}, function(err) {
						$state.go('app.servicos');
						$scope.doRefreshServices();
					});
				}, function(err) {
					$scope.doRefreshServices();
				});
			
				$state.go('app.servicos');
            }
			
			$scope.doRefreshServices = function(){
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/services/service?access_token='+data.data.access_token+'&city='+localStorage.getItem('city')).then(function(data){
						$scope.servicos = data.data;
						$scope.$broadcast('scroll.refreshComplete');
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.userImage = 'img/default.png';
			$scope.showUserImageOptions = function(){
				var actionSheet = $ionicActionSheet.show({
					buttons: [
						{ text: 'Selecionar foto da galeria' },
						{ text: 'Tirar Foto' }
					],
					destructiveText: 'Remover',
					titleText: 'Adicione uma foto de perfil',
					cancelText: 'Cancelar',
					cancel: function() {
						// add cancel code..
					},
					buttonClicked: function(index) {
						switch (index){
							case 0:
								var options = {
									maximumImagesCount: 1,
									width: 800,
									height: 800,
									quality: 100
								};
								
								$cordovaImagePicker.getPictures(options).then(function (results) {
									$scope.userImage = results[0];
									$scope.imageDesc = '';
								}, function(error) {
									$ionicPopup.alert({
										title: 'Erro',
										template: 'Ops, não foi possível selecionar a imagem de perfil'
									});
									
									console.log(error);
								});
								
								break;
							case 1:
								var options = {
									quality: 50,
									destinationType: Camera.DestinationType.DATA_URL,
									sourceType: Camera.PictureSourceType.CAMERA,
									allowEdit: true,
									encodingType: Camera.EncodingType.JPEG,
									targetWidth: 100,
									targetHeight: 100,
									popoverOptions: CameraPopoverOptions,
									saveToPhotoAlbum: false,
									correctOrientation:true
								};
								
								$cordovaCamera.getPicture(options).then(function(imageData) {
									$scope.userImage = "data:image/jpeg;base64," + imageData;
									$scope.imageDesc = '';
								}, function(err) {
									console.log(err);
								});
								
								break;
						}
						
						return true;
					},
					destructiveButtonClicked: function(){
						scope.imageDesc = '+';
						$scope.userImage	= '';
					}
				});

			}
			
			$scope.signUp = function(signUpData){
				$ionicLoading.show();
				if(signUpData.ds_senha == signUpData.senha_confirm){
					OAuth.getAccessToken(authData).then(function(data){
						var options = {
							access_token: data.data.access_token,
							fileKey: "file",
							fileName: "teste",
							mimeType: "*/*",
							chunkedMode: true,
							headers: {
								Authorization: data.data.access_token
							}
						};
						
						$cordovaFileTransfer.upload(encodeURI(appConfig.baseUrl+'/api/uploads/image?access_token='+data.data.access_token), $scope.userImage, options).then(function(result) {
							var userData = {
								'name' : signUpData.nome,
								'phone' : signUpData.phone,
								'email' : signUpData.ds_login,
								'password' : signUpData.ds_senha,
								'role': 'user'
							};
							
							userData.image = result.response;
							
							User.save(userData, $scope);
							
							var authData = {
								'grant_type': 'password',
								'username' : signUpData.ds_login,
								'password' : signUpData.ds_senha,
								'client_id': 'appid01',
								'client_secret': 'secret'
							};
							
							localStorage.setItem('user_info', JSON.stringify(authData));
							
							$ionicLoading.hide();
							
							$ionicPopup.alert({
								title: 'Cadastro de Perfil',
								template: 'Seu perfil foi cadastrado com sucesso'
							});
							
							$state.go('app.servicos');
							
							$scope.modalUpdateSignUp.hide();
						}, function(err) {
							console.log(err);
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Erro no Cadastro do Perfil',
								template: 'Verifique se todos os campos foram preenchidos corretamente'
							});
						}, function (progress) {
							console.log(progress);
						});
					});
				}else{
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: 'Atenção',
						template: 'As senhas devem ser iguais'
					});
				}
			}
			
			$ionicModal.fromTemplateUrl('templates/update-user.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalUpdateSignUp = modal;
            })
			
			$ionicModal.fromTemplateUrl('templates/signup.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalSignUp = modal;
			})
			
			window.cordovaOauth = $cordovaOauth;
			window.http = $http;
			
			$scope.fbLogin = function(){
				$cordovaOauth.facebook("884163068364381", ["email", "public_profile"], {redirect_uri: "http://localhost/callback"}).then(function(result){
						$scope.getFacebookData(result.access_token);
				},  function(error){
						console.log(error);
				});
			}
			
			$scope.getFacebookData = function(access_token){
				$ionicLoading.show();
				$http.get("https://graph.facebook.com/v2.2/me", {
					  params: {
						access_token: access_token,
						fields: "id,name,gender,location,picture.type(large)",
						format: "json"
					  }
				}).then(function(result) {
					  localStorage.setItem('facebook_id', result.data.id);
						
					  FacebookUserCheck.get({facebook_id: result.data.id}, function(data){
						$scope.profileImg = result.data.picture.data.url;
						$scope.userName = result.data.name;
						$scope.facebookID = result.data.id;
						
						if(data.length == 0){
							$ionicLoading.hide();
							$scope.modalUpdateSignUp.show();
						}else{
							var name = result.data.name;
							var picture = result.data.picture;
							var user_info = '{ "facebook_id": "'+result.data.id+'", "grant_type": "password", "name": "'+name+'", "username":"'+data[0].email+'",	  "password": "'+data[0].facebook_password+'",	"client_id": "appid01", "client_secret": "secret", "image":"'+picture.data.url+'" }';

							localStorage.setItem('user_info', user_info);
							$state.go('app.servicos');
						}
						
						$ionicLoading.hide();
					  }, function(error){
						console.log(error);
						$ionicLoading.hide();
					  });
						
					  var data = {
						'facebook_id' : result.data.id,
						'name' : name,
						'role': 'user'
					  };
						
				}, function(error) {
					  console.log(error);
				});
			}
			
			$scope.googleLogin = function(){
				$cordovaOauth.google("694820446547-4gnlp34dk0jpsimp5n6mtamha8b6uh2j.apps.googleusercontent.com", ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/userinfo.email"]).then(function(result) {
						$scope.getGoogleData(result.access_token);
				}, function(error) {
						console.log(error);
				});
			}
			
			$scope.getGoogleData = function(access_token){
				$http.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json", {
					 params: {
						  access_token: access_token,
						  fields: "name,gender,email,picture",
						  format: "json"
					 }
				}).then(function(result) {
					var name = result.data.name;
					var gender = result.data.gender;
					var picture = result.data.picture;
						
					var user_info = '{ "user_info" : { "user": "@'+name.replace(' ','').toLowerCase()+'", "username":"'+name+'", "gender":"'+gender+'", "image":"'+picture+'" } }';
						
					localStorage.setItem('user_info', user_info);
					$state.go('app.servicos');
				}, function(error) {
					console.log(error);
				});
			}
			
			$scope.updateSignUp = function(updateSignUpData){
				if(updateSignUpData.senha == updateSignUpData.senha_confirm){
					var data = {
						'facebook_id' : $scope.facebookID,
						'name': $scope.userName,
						'email': updateSignUpData.ds_login,
						'password' : updateSignUpData.senha,
						'image': $scope.profileImg,
						'role': 'user'
					};
			
					FacebookUserInsert.save(data, $scope);
				}else{
					$ionicPopup.alert({
						title: 'Atenção',
						template: 'As senhas devem ser iguais'
					});
				}
			}
			
			$scope.closeModal = function() {
                $scope.modalUpdateSignUp.hide();
                $scope.modalSignUp.hide();
            }
			
			$scope.signUpModal = function(){
                $scope.modalSignUp.show();
			};
})

.controller('AppCtrl', function($scope, $state, $ionicModal, $rootScope, authData, $ionicLoading, $http, OAuth, appConfig, $cordovaGeolocation, $timeout){
			$scope.notifications = function(){
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/notifications/notification?access_token='+data.data.access_token).then(function(data){
						$scope.notificationCounter = 0;
						
						angular.forEach(data.data, function(value, key) {
							value.created_at = moment(new Date(value.created_at.replace(' ', 'T'))).fromNow();
							
							if(value.unread == 0){
								$scope.notificationCounter++;
								value.unread = 'unread';
							}
						});
						
						$scope.notificationItems = data.data;
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$rootScope.$on('$stateChangeSuccess', function(){
				if($state.current.url == '/servicos'){
					$scope.searchBar = 'show';
					searchData.filter = '0';
				}else{
					$scope.searchBar = 'hide';
				}
			});
			
			var posOptions = {timeout: 5000, enableHighAccuracy: true};
			$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
				var lat  = position.coords.latitude
				var long = position.coords.longitude
				
				$http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=-23.574581,-46.6235209&key=AIzaSyCmUWdqp6dWn_8HCR-3nxdhKxGYxgY86qs').then(function(resp) {
					console.log(resp.data.results[9].address_components[0].long_name);
					localStorage.setItem('city', resp.data.results[9].address_components[0].long_name);
					$scope.doRefreshServices();
				}, function(err) {
					$scope.doRefreshServices();
					console.log(err);
				});
			}, function(err) {
				$scope.userInfo = JSON.parse(localStorage.getItem('user_info'));
				console.log(err);
			});
			
			$scope.doRefreshServices = function(){
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/services/service?access_token='+data.data.access_token+'&city='+localStorage.getItem('city')).then(function(data){
						$scope.servicos = data.data;
						$scope.$broadcast('scroll.refreshComplete');
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$timeout(function(){
				$scope.notifications();
				$scope.userInfo = JSON.parse(localStorage.getItem('user_info'));
				$ionicLoading.hide();
			}, 2000);
			
            $ionicModal.fromTemplateUrl('templates/notifications.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
            });
            
            $scope.closeModal = function() {
				$scope.notifications();
                $scope.modal.hide();
            };
            
            $scope.showNotifications = function(){
				$scope.notifications();
                $scope.modal.show();
			
				setTimeout(function(){
					authData = JSON.parse(localStorage.getItem('user_info'));
					OAuth.getAccessToken(authData).then(function(data){
						$http.get(appConfig.baseUrl+'/api/notifications/notification/1?access_token='+data.data.access_token).then(function(data){
							$scope.notifications();
						}, function(error){
							console.log(error);
						});
					});
				}, 10000);
            };
			
			$scope.logout = function(){
				localStorage.removeItem('user_info');
				$state.go('login');
			}
})

.controller('HomeCtrl', function($scope, $state){
            var month;
            
            $scope.dia = moment().lang("pt-br").date();
            
            switch(moment().lang("pt-br").month()){
                case 0: month = 'Jan'; break;
                case 1: month = 'Fev'; break;
                case 2: month = 'Mar'; break;
                case 3: month = 'Abr'; break;
                case 4: month = 'Maio'; break;
                case 5: month = 'Jun'; break;
                case 6: month = 'Jul'; break;
                case 7: month = 'Ago'; break;
                case 8: month = 'Set'; break;
                case 9: month = 'Out'; break;
                case 10: month = 'Nov'; break;
                case 11: month = 'Dez'; break;
            }
            
            $scope.mes = month;
})

.controller('ServicosCtrl', function($scope, $http, $filter, $ionicPopup, $ionicModal, $ionicLoading, OAuth, appConfig, authData, $state, $cordovaGeolocation){
			$ionicLoading.show();
			
			$scope.searchSelect ='hide';
			$scope.priceSelect ='hide';
			$scope.cityInput ='hide';
			
			$scope.allServices = function(){
				var city = localStorage.getItem('city');
				if(!city)
					city = '';
			
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/services/service?access_token='+data.data.access_token+'&city='+city).then(function(data){
						console.log(data);
						
						if(data.data.length == 0)
							$scope.emptyDataMessage = 'show';
						else
							$scope.emptyDataMessage = 'hide';
							
						$scope.servicos = data.data;
						
						localStorage.setItem('services', JSON.stringify(data.data));
						
						$scope.userInfo = JSON.parse(localStorage.getItem('user_info'));
						
						$scope.$broadcast('scroll.refreshComplete');
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.allServices();
			
			var posOptions = {timeout: 5000, enableHighAccuracy: true};
			$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
				var lat  = position.coords.latitude;
				var long = position.coords.longitude;
				
				$http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat.replace(',', '.')+','+long.replace(',', '.')+'&key=AIzaSyCmUWdqp6dWn_8HCR-3nxdhKxGYxgY86qs').then(function(resp) {
					localStorage.setItem('city', resp.data.results[9].address_components[0].long_name);
				}, function(err) {
					$scope.allServices();
				});
			}, function(err) {
				$scope.allServices();
			});
			
			$scope.filterChange = function(searchData){
				$ionicLoading.show();
				$scope.searchSelect ='hide';
				$scope.priceSelect ='hide';
			
				if(searchData.filter != 5){
					$scope.priceSelect ='hide';
					$scope.cityInput ='hide';
			
					if(searchData.filter != 0 && searchData.filter != 6)
						$scope.searchSelect ='show';
					else
						$scope.searchSelect ='hide';
			
					if(searchData.filter == 6){
						$scope.cityInput ='show';
						searchData.search = localStorage.getItem('city');
					}
				}else{
					$scope.priceSelect ='show';
					$scope.searchSelect ='hide';
					$scope.cityInput ='hide';
				}
			
				var city = localStorage.getItem('city');
				if(!city)
					city = '';
			
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl+'/api/filters/filter/'+searchData.filter+'?access_token='+data.data.access_token+'&city='+city).then(function(data){
						$scope.searches = data.data;
						
						$ionicLoading.hide();
					}, function(error){
						console.log(error);
						$ionicLoading.hide();
					});
				});
			}
			
			$scope.filterResultChange = function(searchData){
				if(searchData.filter != 6){
					$ionicLoading.show();
					var city = localStorage.getItem('city');
					if(!city)
						city = '';
				}else{
					var city = searchData.search;
					if(!city)
						city = '';
				}
			
				console.log(searchData);
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl+'/api/searches/search/'+searchData.filter+'?access_token='+data.data.access_token+'&search='+searchData.search+'&city='+city).then(function(data){
						if(data.data.length == 0)
							$scope.emptyDataMessage = 'show';
						else
							$scope.emptyDataMessage = 'hide';
						
						$scope.servicos = data.data;
						
						$ionicLoading.hide();
					}, function(error){
						console.log(error);
						$ionicLoading.hide();
					});
				});
			}
			
			$ionicModal.fromTemplateUrl('templates/invoice.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalInvoice = modal;
			});
			
			$scope.scheduleService = function(service_id){
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					var access_token = data.data.access_token;
					$http.get(appConfig.baseUrl+'/api/services/service/'+service_id+'?access_token='+data.data.access_token).then(function(data){
						$http.get(appConfig.baseUrl + '/api/pets/pet?access_token='+access_token).then(function(data){
							$scope.pets = data.data;
						}, function(error){
							console.log(error);
						});

						$scope.serviceSchedule = data.data[0];
						
						if($scope.serviceSchedule.sale == '0'){
							$scope.isSale = 'hide';
							$scope.isNotSale = 'show';
						}else{
							$scope.isSale = 'show';
							$scope.isNotSale = 'hide';
						}
						
						$scope.getMapPlace();
						$scope.modalInvoice.show();
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.closeInvoiceModal = function() {
                $scope.modalInvoice.hide();
            }
			
			$scope.getMapPlace = function(){
				$http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+$scope.serviceSchedule.address+',+'+$scope.serviceSchedule.number+'+'+$scope.serviceSchedule.district+',+'+$scope.serviceSchedule.city+'&key=AIzaSyCmUWdqp6dWn_8HCR-3nxdhKxGYxgY86qs').then(function(resp) {
					console.log(resp);
					$scope.map = { center: { latitude: resp.data.results[0].geometry.location.lat, longitude: resp.data.results[0].geometry.location.lng }, zoom: 15 };
					$scope.marker = {
						id: 0,
						coords: {
							latitude: resp.data.results[0].geometry.location.lat,
							longitude: resp.data.results[0].geometry.location.lng
						},
						options: { draggable: true }
					};
				}, function(err) {
					console.log(err);
				});
			}
			
			$scope.schedule = function(scheduleData, service_id){
				$ionicLoading.show();
				scheduleData.service_id = service_id;
				scheduleData.date = $filter('date')(scheduleData.date, "yyyy-MM-dd");
				scheduleData.date = scheduleData.date.split(' ')[0];
				if(scheduleData.time){
					scheduleData.date += ' '+scheduleData.time.replace('h', '')+':00:00';
					scheduleData.email = JSON.parse(localStorage.getItem('user_info')).username;
					console.log(scheduleData);
					authData = JSON.parse(localStorage.getItem('user_info'));
					OAuth.getAccessToken(authData).then(function(data){
						$http.defaults.headers.common['Authorization'] = 'Bearer ' + data.data.access_token;
						$http.post(appConfig.baseUrl+'/api/schedules/schedule?access_token='+data.data.access_token, scheduleData).then(function(data){
							scheduleData.date = '';
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Agendamento',
								template: 'Serviço agendado com sucesso'
							});
							$scope.modalInvoice.hide();
							$state.go('app.agendamentos');
						}, function(error){
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Agendamento',
								template: 'Falha no agendamento. Verifique se todos os campos foram preenchidos corretamente'
							});
							console.log(error);
						});
					});
				}else{
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: 'Agendamento',
						template: 'Selecione um horário para que o serviço seja agendado'
					});
				}
			}
			
			$scope.dateChange = function(date){
				if(moment().diff(moment(new Date(date)), 'days') <= 0){
					var selectedDateDay = moment(new Date(date)).format('dddd').substring(0, 3).toUpperCase();
					$scope.times = '';
				
					selectedDateDay = selectedDateDay.replace('Á', 'A');
				
					var timeArray = [];
					if($scope.serviceSchedule.weekdays.indexOf(selectedDateDay) >= 0){
						angular.forEach($scope.serviceSchedule.weekdays_hours.split('|'), function(value, key) {
							timeArray.push(parseInt(value)+'h');
						});
					}else if($scope.serviceSchedule.weekends.indexOf(selectedDateDay) >= 0){
						angular.forEach($scope.serviceSchedule.weekends_hours.split('|'), function(value, key) {
							timeArray.push(parseInt(value)+'h');
						});
					}else{
						$ionicPopup.alert({
							title: 'Atenção',
							template: 'Nenhum horário disponível para a data selecionada'
						});
					}
				
					$scope.times = timeArray;
				}else{
					$scope.times = [];
				}
			}
			
			$scope.petshopPage = function(id){
				$state.go('app.petshop', {petshopId: id});
			}
})

.controller('PetshopCtrl', function($scope, $state, $http, $ionicPopup, $ionicLoading, Petshop, PetshopServices, authData, $stateParams, OAuth, appConfig, $ionicModal){
			$ionicLoading.show();
			
			var authData = JSON.parse(localStorage.getItem('user_info'));
			OAuth.getAccessToken(authData).then(function(data){
				$http.get(appConfig.baseUrl+'/api/petshops/petshop/'+$stateParams.petshopId+'?access_token='+data.data.access_token).then(function(data){
					$scope.isFavorite = data.data[0].favorite;
					$scope.petshop = data.data[0];
					$scope.getMapPlace(data.data[0]);
					$ionicLoading.hide();
				}, function(error){
					console.log(error);
					$ionicLoading.hide();
				});
				
				$http.get(appConfig.baseUrl+'/api/petshops/services/'+$stateParams.petshopId+'?access_token='+data.data.access_token).then(function(data){
					$scope.petshopServices = data.data;
				}, function(error){
					console.log(error);
				});
				
				$http.get(appConfig.baseUrl + '/api/pets/pet?access_token='+data.data.access_token).then(function(data){
					$scope.pets = data.data;
				}, function(error){
					console.log(error);
				});
			});
			
			$scope.getMapPlace = function(petshop){
				$http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+petshop.address+',+'+petshop.number+'+'+petshop.district+',+'+petshop.city+'&key=AIzaSyCmUWdqp6dWn_8HCR-3nxdhKxGYxgY86qs').then(function(resp) {
					$scope.map = { center: { latitude: resp.data.results[0].geometry.location.lat, longitude: resp.data.results[0].geometry.location.lng }, zoom: 15 };
					$scope.marker = {
						id: 0,
						coords: {
							latitude: resp.data.results[0].geometry.location.lat,
							longitude: resp.data.results[0].geometry.location.lng
						},
						options: { draggable: true }
					};
				}, function(err) {
					console.log(err);
				});
			}
			
			$ionicModal.fromTemplateUrl('templates/invoice.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalInvoice = modal;
			});
			
			$scope.closeInvoiceModal = function() {
                $scope.modalInvoice.hide();
            }
			
			$scope.scheduleService = function(service_id, pet_id){
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					var access_token = data.data.access_token;
					$http.get(appConfig.baseUrl+'/api/services/service/'+service_id+'?access_token='+data.data.access_token).then(function(data){
						$http.get(appConfig.baseUrl + '/api/pets/pet?access_token='+access_token).then(function(data){
							$scope.pets = data.data;
						}, function(error){
							console.log(error);
						});

						$scope.serviceSchedule = data.data[0];
						
						if($scope.serviceSchedule.sale == '0'){
							$scope.isSale = 'hide';
							$scope.isNotSale = 'show';
						}else{
							$scope.isSale = 'show';
							$scope.isNotSale = 'hide';
						}
						
						$scope.scheduleData = {};
						$scope.scheduleData.pet_id = pet_id;
						$scope.getMapPlace($scope.petshop);
						$scope.modalInvoice.show();
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.scheduleConfirm = function(scheduleData){
				$scope.scheduleService(scheduleData.service_id, scheduleData.pet_id);
			}
			
			$scope.schedule = function(scheduleData, service_id){
				$ionicLoading.show();
				scheduleData.service_id = service_id;
				scheduleData.date = $filter('date')(scheduleData.date, "yyyy-MM-dd");
				scheduleData.date = scheduleData.date.split(' ')[0];
				if(scheduleData.time){
					scheduleData.date += ' '+scheduleData.time.replace('h', '')+':00:00';
					scheduleData.email = JSON.parse(localStorage.getItem('user_info')).username;
					authData = JSON.parse(localStorage.getItem('user_info'));
					OAuth.getAccessToken(authData).then(function(data){
						$http.post(appConfig.baseUrl+'/api/schedules/schedule?access_token='+data.data.access_token, scheduleData).then(function(data){
							scheduleData.date = '';
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Agendamento',
								template: 'Serviço agendado com sucesso'
							});
							$scope.modalInvoice.hide();
							$state.go('app.agendamentos');
						}, function(error){
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Agendamento',
								template: 'Falha no agendamento. Verifique se todos os campos foram preenchidos corretamente'
							});
							console.log(error);
						});
					});
				}else{
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: 'Agendamento',
						template: 'Selecione um horário para que o serviço seja agendado'
					});
				}
			}
			
			$scope.favoriteService = function(){
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl+'/api/petshops/favorite/'+$stateParams.petshopId+'?access_token='+data.data.access_token).then(function(data){
						console.log(data.data);
						$scope.isFavorite = data.data;
					}, function(error){
						console.log(error);
						$ionicLoading.hide();
					});
				});
			}
			
			$scope.dateChange = function(date){
				if(moment().diff(moment(new Date(date)), 'days') <= 0){
					var selectedDateDay = moment(new Date(date)).format('dddd').substring(0, 3).toUpperCase();
					$scope.times = '';
				
					selectedDateDay = selectedDateDay.replace('Á', 'A');
				
					var timeArray = [];
					if($scope.serviceSchedule.weekdays.indexOf(selectedDateDay) >= 0){
						angular.forEach($scope.serviceSchedule.weekdays_hours.split('|'), function(value, key) {
							timeArray.push(parseInt(value)+'h');
						});
					}else if($scope.serviceSchedule.weekends.indexOf(selectedDateDay) >= 0){
						angular.forEach($scope.serviceSchedule.weekends_hours.split('|'), function(value, key) {
							timeArray.push(parseInt(value)+'h');
						});
					}else{
						$ionicPopup.alert({
							title: 'Atenção',
							template: 'Nenhum horário disponível para a data selecionada'
						});
					}
				
					$scope.times = timeArray;
				}else{
					$scope.times = [];
				}
			}
})

.controller('SchedulesCtrl', function($scope, $ionicLoading, $http, $ionicModal, $ionicPopup, OAuth, authData, appConfig, $filter){
			$ionicLoading.show();
			var authData = JSON.parse(localStorage.getItem('user_info'));
			
			OAuth.getAccessToken(authData).then(function(data){
				$http.get(appConfig.baseUrl + '/api/schedules/schedule?access_token='+data.data.access_token).then(function(data){
					angular.forEach(data.data, function(value, key) {
						switch(value.status){
							case 'Finalizado':
								value.status_text = 'Concluído';
								break;
							case 'Em andamento':
								value.status_text = 'Em andamento · Termina '+moment(new Date(value.end.replace(' ', 'T'))).add(3, 'hours').fromNow();
								break;
							case 'Na fila':
								value.status_text = 'Agendado para '+moment(new Date(value.date.replace(' ', 'T'))).add(3, 'hours').format('llll');
								break;
							case 'Aguardando aprovação':
								value.status_text = 'Em espera para '+moment(new Date(value.date.replace(' ', 'T'))).add(3, 'hours').format('lll');
								break;
							case 'Cancelado':
								value.status_text = 'Cancelado';
								break;
						}
					});
					$scope.schedules = data.data;
					$ionicLoading.hide();
				}, function(error){
					console.log(error);
				});
			});
			
			$scope.doRefresh = function(){
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/schedules/schedule?access_token='+data.data.access_token).then(function(data){
						angular.forEach(data.data, function(value, key) {
							switch(value.status){
								case 'Finalizado':
									value.status_text = 'Concluído';
									break;
								case 'Em andamento':
									value.status_text = 'Em andamento · Termina '+moment(new Date(value.end.replace(' ', 'T'))).add(3, 'hours').fromNow();
									break;
								case 'Na fila':
									value.status_text = 'Agendado para '+moment(new Date(value.date.replace(' ', 'T'))).add(3, 'hours').format('llll');
									break;
								case 'Aguardando aprovação':
									value.status_text = 'Aguardando aprovação para '+moment(new Date(value.date.replace(' ', 'T'))).add(3, 'hours').format('lll');
									break;
								case 'Cancelado':
									value.status_text = 'Cancelado';
									break;
							}
						});
						$scope.schedules = data.data;
						$scope.$broadcast('scroll.refreshComplete');
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.getMapPlace = function(petshop){
				$http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+petshop.address+',+'+petshop.number+'+'+petshop.district+',+'+petshop.city+'&key=AIzaSyCmUWdqp6dWn_8HCR-3nxdhKxGYxgY86qs').then(function(resp) {
					$scope.map = { center: { latitude: resp.data.results[0].geometry.location.lat, longitude: resp.data.results[0].geometry.location.lng }, zoom: 15 };
					$scope.marker = {
						id: 0,
						coords: {
							latitude: resp.data.results[0].geometry.location.lat,
							longitude: resp.data.results[0].geometry.location.lng
						},
						options: { draggable: true }
					};
				}, function(err) {
					console.log(err);
				});
			}
			
			$ionicModal.fromTemplateUrl('templates/schedule-info.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalSchedule = modal;
			});
			
			$scope.closeScheduleModal = function() {
                $scope.modalSchedule.hide();
            }
			
			$scope.scheduleInfo = function(schedule_id) {
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/pets/pet?access_token='+data.data.access_token).then(function(data){
						$scope.pets = data.data;
					}, function(error){
						console.log(error);
					});
					
					$http.get(appConfig.baseUrl+'/api/schedules/schedule/'+schedule_id+'?access_token='+data.data.access_token).then(function(data){
						$scope.serviceSchedule = data.data[0];
						
						if($scope.serviceSchedule.status == 'Na fila' || $scope.serviceSchedule.status == 'Aguardando aprovação'){
							$scope.scheduleData = {};
							$scope.scheduleData = $scope.serviceSchedule;
							$scope.scheduleData.time = moment($scope.serviceSchedule.date).calendar();
							$scope.scheduleData.date = moment($scope.serviceSchedule.date).format('LL');
							
							$scope.getMapPlace(data.data[0]);
							$scope.modalSchedule.show();
						}
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$ionicModal.fromTemplateUrl('templates/invoice.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalInvoice = modal;
			});
			
			$scope.closeInvoiceModal = function() {
                $scope.modalInvoice.hide();
            }
			
			$scope.reschedule = function(scheduleData) {
				var confirmPopup = $ionicPopup.confirm({
				 title: 'Agendamento',
				 template: 'Você tem certeza que deseja reagendar o serviço? <b>Este agendamento será cancelado e será criado um novo</b>',
				 buttons: [
				  { text: 'Cancelar' },
				  {
					text: '<b>Sim</b>',
					onTap: function(e) {
					  $ionicLoading.show();
					  OAuth.getAccessToken(authData).then(function(data){
						$http.delete(appConfig.baseUrl + '/api/schedules/schedule/'+scheduleData.schedule_id+'?access_token='+data.data.access_token).then(function(data){
							$scope.doRefresh();
							$ionicLoading.hide();
							$scope.modalSchedule.hide();
							$scope.scheduleService(scheduleData.service_id);
						}, function(error){
							console.log(error);
							$ionicLoading.hide();
							$ionicPopup.alert({
									title: 'Agendamento',
									template: 'Não foi possível reagendar o serviço'
							});
						});
					  });
					}
				  }
				 ]
			   });
			}
			
			$scope.scheduleService = function(service_id){
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl+'/api/services/service/'+service_id+'?access_token='+data.data.access_token).then(function(data){
						$scope.serviceSchedule = data.data[0];
						$scope.getMapPlace(data.data[0]);
						$scope.modalInvoice.show();
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.schedule = function(scheduleData, service_id){
				$ionicLoading.show();
				scheduleData.service_id = service_id;
				scheduleData.date = $filter('date')(scheduleData.date, "yyyy-MM-dd");
				scheduleData.date = scheduleData.date.split(' ')[0];
				if(scheduleData.time){
					scheduleData.date += ' '+scheduleData.time.replace('h', '')+':00:00';
					scheduleData.email = JSON.parse(localStorage.getItem('user_info')).username;
					authData = JSON.parse(localStorage.getItem('user_info'));
					OAuth.getAccessToken(authData).then(function(data){
						$http.post(appConfig.baseUrl+'/api/schedules/schedule?access_token='+data.data.access_token, scheduleData).then(function(data){
							scheduleData.date = '';
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Agendamento',
								template: 'Serviço agendado com sucesso'
							});
							$scope.modalInvoice.hide();
							$state.go('app.agendamentos');
							$scope.doRefresh();
						}, function(error){
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Agendamento',
								template: 'Falha no agendamento. Verifique se todos os campos foram preenchidos corretamente'
							});
							console.log(error);
						});
					});
				}else{
					$ionicLoading.hide();
					$ionicPopup.alert({
						title: 'Agendamento',
						template: 'Selecione um horário para que o serviço seja agendado'
					});
				}
			}
			
			$scope.dateChange = function(date){
				if(moment().diff(moment(new Date(date)), 'days') <= 0){
					var selectedDateDay = moment(new Date(date)).format('dddd').substring(0, 3).toUpperCase();
					$scope.times = '';
				
					selectedDateDay = selectedDateDay.replace('Á', 'A');
				
					var timeArray = [];
					if($scope.serviceSchedule.weekdays.indexOf(selectedDateDay) >= 0){
						angular.forEach($scope.serviceSchedule.weekdays_hours.split('|'), function(value, key) {
							timeArray.push(parseInt(value)+'h');
						});
					}else if($scope.serviceSchedule.weekends.indexOf(selectedDateDay) >= 0){
						angular.forEach($scope.serviceSchedule.weekends_hours.split('|'), function(value, key) {
							timeArray.push(parseInt(value)+'h');
						});
					}else{
						$ionicPopup.alert({
							title: 'Atenção',
							template: 'Nenhum horário disponível para a data selecionada'
						});
					}
				
					$scope.times = timeArray;
				}else{
					$scope.times = [];
				}
			}
			
			$scope.cancel = function(scheduleData) {
				var confirmPopup = $ionicPopup.confirm({
				 title: 'Agendamento',
				 template: 'Você tem certeza que deseja cancelar este agendamento?',
				 buttons: [
				  { text: 'Cancelar' },
				  {
					text: '<b>Sim</b>',
					onTap: function(e) {
					  $ionicLoading.show();
					  OAuth.getAccessToken(authData).then(function(data){
						$http.delete(appConfig.baseUrl + '/api/schedules/schedule/'+scheduleData.schedule_id+'?access_token='+data.data.access_token).then(function(data){
							$scope.doRefresh();
							$scope.modalSchedule.hide();
							$ionicLoading.hide();
							$ionicPopup.alert({
									title: 'Agendamento',
									template: 'Serviço agendado com sucesso'
							});
						}, function(error){
							console.log(error);
							$ionicLoading.hide();
							$ionicPopup.alert({
									title: 'Agendamento',
									template: 'Não foi possível reagendar o serviço'
							});
						});
					  });
					}
				  }
				 ]
			   });
			}
})

.controller('PetsCtrl', function($scope, $state, $http, $ionicPopup, $ionicModal, $ionicLoading, OAuth, appConfig, authData, $ionicActionSheet, $cordovaImagePicker, $cordovaCamera, $cordovaFileTransfer){
			$ionicLoading.show();
			$scope.imageDesc = '+ Adicione a foto do seu pet';
			$scope.petImageHeight = 'auto';
			
			var authData = JSON.parse(localStorage.getItem('user_info'));
			
			OAuth.getAccessToken(authData).then(function(data){
				$http.get(appConfig.baseUrl + '/api/pets/pet?access_token='+data.data.access_token).then(function(data){
					$scope.pets = data.data;
					$ionicLoading.hide();
				}, function(error){
					console.log(error);
				});
			});
			
			$scope.doRefresh = function(){
				authData = JSON.parse(localStorage.getItem('user_info'));
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/pets/pet?access_token='+data.data.access_token).then(function(data){
						$scope.pets = data.data;
						$ionicLoading.hide();
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$ionicModal.fromTemplateUrl('templates/create-pet.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalCreatePet = modal;
			});
			
			$ionicModal.fromTemplateUrl('templates/edit-pet.html', {
				scope: $scope
			}).then(function(modal) {
				$scope.modalEditPet = modal;
			});
			
			$scope.savePet = function(petData){
				$ionicLoading.show();
				OAuth.getAccessToken(authData).then(function(data){
					var options = {
						access_token: data.data.access_token,
						fileKey: "file",
						fileName: "teste",
						mimeType: "*/*",
						chunkedMode: true,
						headers: {
							Authorization: data.data.access_token
						}
					};
					
					switch(petData.animal){
						case 'Gato': petData.breed = petData.catBreed; break;
						case 'Outro': petData.breed = petData.otherAnimal; break;
					}
					
					$cordovaFileTransfer.upload(encodeURI(appConfig.baseUrl+'/api/uploads/image?access_token='+data.data.access_token), $scope.petImage, options).then(function(result) {
						petData.image = result.response;
						$http.put(appConfig.baseUrl+'/api/pets/pet/'+petData.id+'?access_token='+data.data.access_token, petData).then(function(data){
							$ionicPopup.alert({
								title: 'Edição do Pet',
								template: 'Pet editado com sucesso'
							});
							$scope.modalEditPet.hide();
							$scope.doRefresh();
							$ionicLoading.hide();
						}, function(error){
							console.log(error);
						});
					}, function(err) {
						console.log(err);
						$http.put(appConfig.baseUrl+'/api/pets/pet/'+petData.id+'?access_token='+data.data.access_token, petData).then(function(data){
							$ionicPopup.alert({
								title: 'Edição do Pet',
								template: 'Pet editado com sucesso'
							});
							$scope.modalEditPet.hide();
							$scope.doRefresh();
							$ionicLoading.hide();
						}, function(error){
							console.log(error);
						});
					}, function (progress) {
						console.log(progress);
					});
				});
			
			}
			
			$scope.petEdit = function(id){
				$ionicLoading.show();
				OAuth.getAccessToken(authData).then(function(data){
					$http.get(appConfig.baseUrl + '/api/pets/pet/'+id+'?access_token='+data.data.access_token).then(function(data){
						$scope.petData = data.data[0];
						$scope.petImage = $scope.petData.image;
						switch($scope.petData.animal){
							case 'Cão': $scope.dogBreedShow = 'show'; $scope.catBreedShow = 'hide'; $scope.otherAnimalShow = 'hide'; break;
							case 'Gato': $scope.dogBreedShow = 'hide'; $scope.catBreedShow = 'show'; $scope.otherAnimalShow = 'hide'; break;
							case 'Outro': $scope.dogBreedShow = 'hide'; $scope.catBreedShow = 'hide'; $scope.otherAnimalShow = 'show'; break;
						}
						$scope.modalEditPet.show();
						$ionicLoading.hide();
					}, function(error){
						console.log(error);
					});
				});
			}
			
			$scope.removePet = function(id){
				$ionicLoading.show();
				OAuth.getAccessToken(authData).then(function(data){
					$http.delete(appConfig.baseUrl + '/api/pets/pet/'+id+'?access_token='+data.data.access_token).then(function(data){
						$scope.doRefresh();
						$ionicLoading.hide();
						$ionicPopup.alert({
								title: 'Remoção do Pet',
								template: 'Pet removido com sucesso'
						});
					}, function(error){
						console.log(error);
						$ionicLoading.hide();
						$ionicPopup.alert({
								title: 'Remoção do Pet',
								template: 'Não foi possível remover o pet'
						});
					});
				});
			}
			
			$scope.createPetModal = function(){
				$scope.modalCreatePet.show();
			
				$scope.petImage = '';
			}
			
			$scope.closeCreatePetModal = function(){
                $scope.modalCreatePet.hide();
            }

			$scope.closeEditPetModal = function(){
                $scope.modalEditPet.hide();
            }
			
			$scope.showPetImageOptions = function(){
				var actionSheet = $ionicActionSheet.show({
					buttons: [
						{ text: 'Selecionar foto da galeria' },
						{ text: 'Tirar Foto' }
					],
					destructiveText: 'Remover',
					titleText: 'Adicione uma foto do seu pet',
					cancelText: 'Cancelar',
					cancel: function() {
						// add cancel code..
					},
					buttonClicked: function(index) {
						switch (index){
							case 0:
								var options = {
									maximumImagesCount: 1,
									width: 800,
									height: 800,
									quality: 70
								};
								
								$cordovaImagePicker.getPictures(options).then(function (results) {
									$scope.petImageHeight = '100px';
									$scope.petImage = results[0];
									$scope.imageDesc = '';
								}, function(error) {
									$ionicPopup.alert({
										title: 'Erro',
										template: 'Ops, não foi possível selecionar a imagem do seu pet'
									});
									
									console.log(error);
								});
								
								break;
							case 1:
								var options = {
									quality: 50,
									destinationType: Camera.DestinationType.DATA_URL,
									sourceType: Camera.PictureSourceType.CAMERA,
									allowEdit: true,
									encodingType: Camera.EncodingType.JPEG,
									targetWidth: 100,
									targetHeight: 100,
									popoverOptions: CameraPopoverOptions,
									saveToPhotoAlbum: false,
									correctOrientation:true
								};
								
								$cordovaCamera.getPicture(options).then(function(imageData) {
									$scope.petImageHeight = '100px';
									$scope.petImage = "data:image/jpeg;base64," + imageData;
									$scope.imageDesc = '';
								}, function(err) {
									console.log(err);
								});
								
								break;
						}
						
						return true;
					},
					destructiveButtonClicked: function(){
						scope.imageDesc = '+ Adicione a foto do seu pet';
						$scope.petImage	= '';
						$scope.petImageHeight = 'auto';
					}
				});
			}
			
			$scope.createPet = function(petData){
				var authData = JSON.parse(localStorage.getItem('user_info'));
			
				$ionicLoading.show();
				OAuth.getAccessToken(authData).then(function(data){
					var options = {
						access_token: data.data.access_token,
						fileKey: "file",
						fileName: "teste",
						mimeType: "*/*",
						chunkedMode: true,
						headers: {
							Authorization: data.data.access_token
						}
					};
					
					switch(petData.animal){
						case 'Gato': petData.breed = petData.catBreed; break;
						case 'Outro': petData.breed = petData.otherAnimal; break;
					}
					
					$cordovaFileTransfer.upload(encodeURI(appConfig.baseUrl+'/api/uploads/image?access_token='+data.data.access_token), $scope.petImage, options).then(function(result) {
						petData.image = result.response;
						$http.post(appConfig.baseUrl+'/api/pets/pet?access_token='+data.data.access_token, petData).then(function(data){
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Cadastro de Pet',
								template: 'Pet cadastrado com sucesso'
							});
							$scope.modalCreatePet.hide();
							$scope.doRefresh();
						}, function(error){
							console.log(error);
						});
					}, function(err) {
						console.log(err);
						$ionicLoading.hide();
						$ionicPopup.alert({
							title: 'Erro no Cadastro do Pet',
							template: 'Verifique se todos os campos foram preenchidos corretamente'
						});
					}, function (progress) {
						console.log(progress);
					});
				});
			}
			
			$scope.animalChange = function(animal){
				$ionicLoading.show();
				switch(animal){
					case 'Cão': $scope.dogBreedShow = 'show'; $scope.catBreedShow = 'hide'; $scope.otherAnimalShow = 'hide'; break;
					case 'Gato': $scope.dogBreedShow = 'hide'; $scope.catBreedShow = 'show'; $scope.otherAnimalShow = 'hide'; break;
					case 'Outro': $scope.dogBreedShow = 'hide'; $scope.catBreedShow = 'hide'; $scope.otherAnimalShow = 'show'; break;
				}
			
				setTimeout(function(){$ionicLoading.hide()}, 2000);
			}
});