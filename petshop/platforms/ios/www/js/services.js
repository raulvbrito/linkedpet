angular.module('starter.services')

.factory('User', function($state, appConfig, $resource, $ionicLoading, TokenHandler){
	return{
		save: function(data, scope){
			
			var user = $resource(appConfig.baseUrl + '/api/users/user');

			user = TokenHandler.wrapActions(user, ["save"], data, 'Email já cadastrado', 'app.services', scope);
			
			user.save({}, data);

			return user;
		}
	}
})

.factory('FacebookUserCheck', function($state, appConfig, $resource, TokenHandler){
	var facebookUser = $resource(appConfig.baseUrl + '/api/users/facebookUser/'+localStorage.getItem('facebook_id'), [], {
		get: {
			isArray: true
		}
	});
	
	facebookUser = TokenHandler.wrapActions(facebookUser, ["get"]);

	return facebookUser;
})

.factory('FacebookUserInsert', function($state, appConfig, $resource, $ionicLoading, TokenHandler){
	return{
		save: function(data, scope){
			$ionicLoading.show();
			
			var user = $resource(appConfig.baseUrl + '/api/users/facebookUser');

			user = TokenHandler.wrapActions(user, ["save"], data, 'Email já cadastrado', 'app.servicos', scope);
			
			user.save({}, data);

			return user;
		}
	}
})

.factory('Service', function($state, appConfig, $resource, TokenHandler){
	var service = $resource(appConfig.baseUrl + '/api/services/service', [], {
		query: {
			isArray: true
		}
	});
	
	service = TokenHandler.wrapActions(service, ["query"]);

	return service;
})

.factory('ServiceSchedule', function($state, appConfig, $resource, TokenHandler){
	var service_id = localStorage.getItem('service_id');
	var serviceSchedule = $resource(appConfig.baseUrl + '/api/services/service/' + service_id, [], {
		get: {
			isArray: true
		}
	});
	
	serviceSchedule = TokenHandler.wrapActions(serviceSchedule, ["get"]);

	return serviceSchedule;
})

.factory('Petshop', function($state, appConfig, $resource, TokenHandler, $stateParams){
	console.log($stateParams.petshopId);
	var petshop = $resource(appConfig.baseUrl + '/api/petshops/petshop/' + $stateParams.petshopId, [], {
		get: {
			isArray: true
		}
	});
	
	petshop = TokenHandler.wrapActions(petshop, ["get"]);

	return petshop;
})

.factory('PetshopServices', function($state, appConfig, $resource, TokenHandler, $stateParams){
	var petshopServices = $resource(appConfig.baseUrl + '/api/petshops/services/' + $stateParams.petshopId, [], {
		query: {
			isArray: true
		}
	});
	
	petshopServices = TokenHandler.wrapActions(petshopServices, ["query"]);

	return petshopServices;
})

.factory('Schedule', function($state, appConfig, $resource, TokenHandler, $stateParams){
	var schedule = $resource(appConfig.baseUrl + '/api/schedules/schedule', [], {
		get: {
			isArray: true
		}
	});
	
	schedule = TokenHandler.wrapActions(schedule, ["get"]);

	return schedule;
})

.factory('ScheduleSave', function($state, appConfig, $resource, $ionicLoading, TokenHandler, $stateParams){
	return{
		save: function(data, scope){
			$ionicLoading.show();
			
			console.log(data);
			
			var scheduleSave = $resource(appConfig.baseUrl + '/api/schedules/schedule');

			scheduleSave = TokenHandler.wrapActions(scheduleSave, ["save"], data, 'Serviço agendado com sucesso', 'app.servicos', scope);
			
			scheduleSave.save({}, data);

			return scheduleSave;
		}
	}
})

.factory('TokenHandler', function($rootScope, $state, $ionicModal, $ionicLoading, $ionicPopup, OAuth) {
	var tokenHandler = {};
	var token;
	
	var authData = JSON.parse(localStorage.getItem('user_info'));
	if(!authData && $state.current.name == 'login'){
		authData = {
			'grant_type': 'password',
			'username' : 'raulvbrito@gmail.com',
			'password' : '12345',
			'client_id': 'appid01',
			'client_secret': 'secret'
		};
	}
	
	tokenHandler.wrapActions = function(resource, actions, data, errorText, successRoute, scope) {
		var wrappedResource = resource;
		for (var i=0; i < actions.length; i++) {
			tokenWrapper(wrappedResource, actions[i], data, errorText, successRoute, scope);
		};
		return wrappedResource;
	};
	
	var tokenWrapper = function(resource, action, requestData, errorText, successRoute, scope) {
		resource['_' + action] = resource[action];
		resource[action] = function(data, success, error){
			OAuth.getAccessToken(authData).then(function(data){
				token = data.data.access_token;
				if(!requestData){
					return resource['_' + action](
						angular.extend({}, data || {},
						{access_token: token}),
						success,
						error
					);
				}else{
					return resource['_' + action](
						angular.extend({}, requestData || data || {},
						{access_token: token}),
						function(success){
							$ionicLoading.hide();
							if(successRoute == 'app.servicos'){
								localStorage.setItem('user_info', JSON.stringify(authData));
								$state.go(successRoute);
								scope.modalSignUp.hide();
								scope.modalUpdateSignUp.hide();
							}else if(successRoute == 'app.servicos'){
								$ionicPopup.alert({
									title: 'Agendamento',
									template: errorText
								});
								scope.modalInvoice.hide();
							}
						
							return success;
						},
						function(error){
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Atenção',
								template: errorText
							});
						
							return error;
						}
					);
				}
			}, function(error){
				console.log(error);
			});
		};
	};

	return tokenHandler;
});